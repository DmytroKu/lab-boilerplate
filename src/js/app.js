import { randomUserMock, additionalUsers } from './mock_for_L3';
import { modifyUsersFromMock, combineTwoArrays } from './Task1';
import { validate } from './Task2';
import filterArray from './Task3';
import sortArray from './Task4';
import findParticularUser from './Task5';
import percentageOf from './Task6';

// task1
const modified = modifyUsersFromMock(randomUserMock);
// modified.forEach(element => console.log(element));

const modifiedCombined = combineTwoArrays(modified, additionalUsers);
// modifiedCombined.forEach(element => console.log(element));

// task2

validate(modifiedCombined[0]);

// task3
const filteredUsers = filterArray(modifiedCombined, 'country', 'Ireland');
filteredUsers.forEach((element) => console.log(element));

// task4
sortArray(modifiedCombined, 'age', false);
// modifiedCombined.forEach(element => console.log(element));

// task5
const particUser = findParticularUser(modifiedCombined, 'full_name', 'Aaron Enoksen');
console.log(particUser);

// task6
console.log(percentageOf(modifiedCombined, 'age', 67));
