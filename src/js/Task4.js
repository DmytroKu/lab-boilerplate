export default function sortArray(arr, property, ascending) {
  const sortParams = 'full_name, age, b_day, country'.split(', ');
  if (!sortParams.includes(property)) {
    return;
  }
  if (ascending) {
    arr.sort((a, b) => a[property] > b[property]);
  } else {
    arr.sort((a, b) => a[property] < b[property]);
  }
}
