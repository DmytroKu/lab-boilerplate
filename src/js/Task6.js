export default function percentageOf(arr, property, value) {
  const filterParams = 'name, note, age'.split(', ');
  if (!filterParams.includes(property)) {
    return 0;
  }
  const filtered = arr.filter((item) => item[property] === value);
  return (filtered.length / arr.length) * 100;
}
