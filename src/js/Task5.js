export default function findParticularUser(array, field, param) {
  const particularUser = array.find((user) => user[field] === param);
  if (particularUser) return particularUser;
  return null;
}
