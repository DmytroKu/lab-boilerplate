export default function filterArray(arr, filterParam, paramValue) {
  const allowedParams = 'country, age, gender, favorite'.split(', ');
  if (!allowedParams.includes(filterParam)) {
    return [];
  }
  return arr.filter((item) => item[filterParam] === paramValue);
}
