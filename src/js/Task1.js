export function modifyUsersFromMock(randomUserMock) {
  const randomUser = randomUserMock.map((person) => ({
    gender: person.gender,
    title: person.name.title,
    full_name: `${person.name.first} ${person.name.last}`,
    city: person.location.city,
    state: person.location.state,
    country: person.location.country,
    postcode: person.location.postcode,
    coordinates: person.location.coordinates,
    timezone: person.location.timezone,
    email: person.email,
    b_date: person.dob.date,
    age: person.dob.age,
    phone: person.phone,
    picture_large: person.picture.large,
    picture_thumbnail: person.picture.thumbnail,
    id: Math.floor(Math.random() * 100),
    favorite: false,
    course: Math.floor(Math.random() * 5),
    bg_color: '#1f75cb',
    note: 'Some Note',
  }));
  return randomUser;
}

export function combineTwoArrays(RandomUserMock, AdditionalUsers) {
  AdditionalUsers.forEach((el) => {
    if (!RandomUserMock.find((obj) => el.fullname === obj.full_name)) RandomUserMock.push(el);
  });
  return RandomUserMock;
}
