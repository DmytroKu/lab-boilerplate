import phone from 'phone';
import countries from './countries.json';

function validateStrings(obj) {
  let check = true;
  const strings = ['full_name', 'title', 'note', 'state', 'city', 'country'];
  for (let i = 0; i < strings.length; i += 1) {
    if (typeof obj[strings[i]] === 'string' && obj[strings[i]][0].toUpperCase() === obj[strings[i]][0]) {
      check = true;
    } else {
      check = false;
      break;
    }
  }
  return check;
}

function validateAge(obj) {
  return typeof (obj.age) === 'number';
}
export function getCountryCode(countryName) {
  const country = countries.find((element) => element.name === countryName);
  return country['alpha-3'];
}

function validatePhoneNumber(obj) {
  return phone(obj.phone, getCountryCode(obj.country)).length === 2;
}

function validateEmail(obj) {
  return !!obj.email.includes('@');
}
export function validate(object) {
  if (validateStrings(object) && validateAge(object)
  && validatePhoneNumber(object) && validateEmail(object)) {
    console.log('Validation has been passed successfully');
  } else {
    console.log('Validadion has been failed');
  }
}
